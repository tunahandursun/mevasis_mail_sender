#-*- coding: utf8 -*-
import smtplib
import sys
import getopt

try:
    opts, args = getopt.getopt(sys.argv[1:],"m:c:s:",["maillist=","content="])
except getopt.GetoptError:
    print("Parametre hatası\nkullanım: -m mail_adreslistesi -c mail_içeriği")
    sys.exit(1)

for option,argument in opts:
    if option in ("-c","--content"):
        filename_of_content = argument
    elif option in ("-m","--maillist"):
        filename_of_maillist = argument
    elif option in ("-s","--subject"):
        subject_of_mail = argument
try:
    subject_of_mail
except NameError:
    print("-s :: Lütfen subject belirtiniz")
    sys.exit(1)
try:
    file_of_content = open(filename_of_content,"r")
except FileNotFoundError:
    print("-c :: Mail içeriğine ait dosya bulunamadı.")
    sys.exit()
try:
    file_of_maillist = open(filename_of_maillist,"r")
except FileNotFoundError:
    print("-m :: Mail listesine ait dosya bulunamadı")
    sys.exit()

list_of_usernamemail_tuple = []

for username_mail in file_of_maillist.readlines():
    deleted_backn = username_mail.strip('\n')
    tuple_of_usernamemail = tuple(deleted_backn.split(','))
    list_of_usernamemail_tuple.append(tuple_of_usernamemail)

html_content = file_of_content.read()
username = 'fuckin@gmail.com'
password = 'password'
fromaddr = 'fromwho@gmail.com'
server = smtplib.SMTP_SSL('smtp.gmail.com',465) # ssl olmayanlarda stmtplib.SMTP('servername',port) şeklinde de kullanılır
server.ehlo() # ssl değilsen server.starttls() kullan
server.login(username,password)

for name, emailaddress in list_of_usernamemail_tuple:
    toaddrs  = emailaddress
    msg = "Subject: {}\nContent-type: text/html\n\nMerhaba {},\n{}".format(subject_of_mail,name.title(),html_content)
    server.sendmail(fromaddr, toaddrs, msg)

server.quit()
